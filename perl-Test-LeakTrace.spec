Name:		perl-Test-LeakTrace
Summary:	Trace memory leaks
Version:	0.17
Release:	2
License:	GPL-1.0-or-later OR Artistic-1.0-Perl
URL:		https://metacpan.org/release/Test-LeakTrace
Source0:	https://cpan.metacpan.org/authors/id/L/LE/LEEJO/Test-LeakTrace-%{version}.tar.gz

BuildRequires:	coreutils findutils gcc	make perl-interpreter perl-devel perl-generators
BuildRequires:	perl(ExtUtils::MakeMaker) sed

# Don't provide private perl libs
%{?perl_default_filter}

%description
Test::LeakTrace - Traces memory leaks
Test::LeakTrace::Script - A LeakTrace interface for whole scripts

%package_help

%prep
%autosetup -n Test-LeakTrace-%{version}

find . -type f ! -name \*.pl -print0 | xargs -0 chmod 644

# Fix up shellbangs in doc scripts
sed -i -e 's|^#!perl|#!/usr/bin/perl|' benchmark/*.pl example/*.{pl,t} {t,xt}/*.t

%build
perl Makefile.PL INSTALLDIRS=vendor NO_PACKLIST=1 OPTIMIZE="%{optflags}"
%make_build

%install
make pure_install DESTDIR=%{buildroot}
%{_fixperms} -c %{buildroot}

%check
make test

%files
%{perl_vendorarch}/auto/Test/
%{perl_vendorarch}/Test/

%files help
%doc Changes README benchmark/ example/
%{_mandir}/man3/Test::LeakTrace.3*
%{_mandir}/man3/Test::LeakTrace::JA.3*
%{_mandir}/man3/Test::LeakTrace::Script.3*

%changelog
* Sun Jan 19 2025 Funda Wang <fundawang@yeah.net> - 0.17-2
- drop useless perl(:MODULE_COMPAT) requirement

* Fri Jan 29 2021 liudabo <liudabo1@huawei.com> - 0.17-1
- upgrade version to 0.17

* Fri Feb 28 2020 openEuler Buildteam <buildteam@openeuler.org> - 0.16-10
- Package init
